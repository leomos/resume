pandoc \
    --standalone \
    --self-contained \
    --from markdown \
    --to html \
    -c style.css \
    -o resume.html \
    resume.md

wkhtmltopdf --enable-local-file-access \
    --disable-smart-shrinking \
    --zoom 0.82 \
    --margin-top 1mm \
    --margin-bottom 1mm \
    --margin-right 0mm \
    --margin-left 0mm \
    resume.html resume.pdf
