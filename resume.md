# Leonardo Mosciatti

leonardo.mosciatti@gmail.com | +39 328 9221302 | Dublin, Ireland |&nbsp;[https://github.com/leomos](https://github.com/leomos)


## Professional experience

---

<span class="pe-title">
    <span class="pe-company">[Amazon](https://aws.amazon.com/)</span>
    <span class="pe-job">Software Development Engineer</span>
    <span class="pe-date">07/2022 - present</span>
    <span class="pe-location">Dublin, Ireland</span>
</span>

- Designed and built network simulator to check that switch/router configurations were applied correctly using **cRPD** and **Docker** networks.
- Developed features for rack bootstrapping software.
- Automated rack bootstrapping software generation via **CodeBuild**/**CodePipeline** pipelines.

<span class="pe-title">
    <span class="pe-company">[Cloudflare](https://www.cloudflare.com/)</span>
    <span class="pe-job">Network Automation Engineer</span>
    <span class="pe-date">10/2021 - 06/2022</span>
    <span class="pe-location">Lisbon, Portugal</span>
</span>

- Improved custom **salt** modules to automate **BGP** routes announcements.
- Ported legacy codebase from **Python 2.7** to **Python 3**.
- Developed pipeline to simulate production-like environments during tests.

<span class="pe-title">
    <span class="pe-company">[NTT Data](https://it.nttdata.com/)</span>
    <span class="pe-job">Software Engineer</span>
    <span class="pe-date">10/2019 - 10/2021</span> 
    <span class="pe-location">Milan, Italy</span>
</span>

- Developed software to manage, simulate and render hydraulic networks via web interface. 
The software is made with a **microservice architecture**. 
The microservices are developed using **Java** (with **Spring Boot** framework) and **Python** (with **Flask** framework) backed by **PostgreSQL**.
- Created **JNI** library to use a legacy **C** toolkit from Java.
- Developed embedded firmware for custom board equipped with ARM Cortex M4 and **FreeRTOS** kernel. 
- Heavily reduced latency by an order of magnitude in the core section of an IoT system.
- Tutored an intern during his graduate thesis on IoT systems and Data Science.
- Managed **OpenShift** cluster for production deployments.


<span class="pe-title">
    <span class="pe-company">[Gemino](http://www.gemino-srl.com/)</span>
    <span class="pe-job">Software Engineer Intern</span>
    <span class="pe-date">10/2018 - 3/2019</span>
    <span class="pe-location">Milan, Italy</span>
</span>

- Developed web apps to monitor energy consumption of industrial machines. 
Backend was written in **Java** and OSGi framework, while frontend was written using **React**.
Deployment was done on a custom board equipped with an ARM Cortex A53, 
so performance and reliability were major concerns.

<span class="pe-title">
    <span class="pe-company">~~[dyblab](https://dyblab.com)~~</span>
    <span class="pe-job">Web Developer</span>
    <span class="pe-date">10/2016 - 12/2019</span>
    <span class="pe-location">Milan, Italy</span>
</span>

- Developed full-stack web applications using Python/Django and **React**.
- Deployed apps using **Ansible** and **Docker** to privately owned VPSes.

## Skills

---

- *Consolidated knowledge*: Java, Python, JavaScript, Linux, Docker
- *Currently focused on*: Go, Rust


## Education

---

BSc in Computer Science and Engineering, Politecnico di Milano, 2015 - 2020

AWS Certified Developer - Associate Level


## Projects

---

* [dwgd](https://github.com/leomos/dwgd)

  dwgd is a Docker plugin that let your containers connect to a WireGuard network. This is achieved by moving a WireGuard network interface from dwgd running namespace into the designated container namespace.

